package com.jobo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jobo.model.User;
import com.jobo.repository.UserRepository;


@RestController
@RequestMapping("/user")
public class UserService {
	
	private final UserRepository userRepository;
	
	@Autowired
	public UserService (UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void addUser(@RequestBody User user){
		if(userRepository.findByUserName(user.getUsername()) == null)
			if(userRepository.findByEmail(user.getEmail()).isEmpty())
				userRepository.save(user);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<User> getUsers(){
		return userRepository.findAll();
	}
	

}
