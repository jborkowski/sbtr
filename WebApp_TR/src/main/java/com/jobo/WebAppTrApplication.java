package com.jobo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class WebAppTrApplication {
	
    public static void main(String[] args) {
        SpringApplication.run(WebAppTrApplication.class, args);
    }
    
    
}
