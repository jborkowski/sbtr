package com.jobo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.jobo.model.User;

public interface UserRepository extends MongoRepository<User, String>{
	List<User> findByEmail(String email);
	//List<User> findByUserName(String userName);
	User findByUserName(String userName);
	
	@Query("{'userName' : ?0, 'password' : ?1}")
	User findByPassAndName(String userName, String pass);
}
