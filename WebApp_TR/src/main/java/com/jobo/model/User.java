package com.jobo.model;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;



public class User{


	@Id
	private String _id;
	
	private String userName;
	private String firstName;
	private String lastName;
	private List<String> roles = new ArrayList<>();
	private String email;
	private String password;
	private Boolean active;
	private Boolean baned;

	public String get_id() {
		return _id;
	}

	
	public String getUsername() {
		return userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	
	public boolean isEnabled() {
		return active;
	}

	
	public boolean isAccountNonLocked() {
		return !baned;
	}
	

	public List<GrantedAuthority> getRoles() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(roles.size());
	    for (String role : roles) {
	    	authorities.add(new SimpleGrantedAuthority(role));
	    }
		
		return authorities;
	}
	
	
	


}
